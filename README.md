# django-vuejs-translate

This project aims to translate messages inside vuejs code.

Initially this idea and code was designed by [Alex Tkachenko](https://gitlab.com/preusx) 


### Generating translations

```python
python manage.py jsmakemessages -v3 -e jinja,py,html,js,vue -jse js,vue -i node_modules && python manage.py makemessages -v3 -e jinja,py,html,js,vue -i node_modules
```

### Installation

Update INSTALLED_APPS

```python
INSTALLED_APPS = [
    ...
    'vuejs_translate',
    ...
]
```

Add urls

```python
urlpatterns += i18n_patterns(
    path('vuejs-translate', include('vuejs_translate.urls')),
)
```
    

Add script to templates

```html
<script type="text/javascript" src!=url('"vuejs_translate:js-i18n"')>
```

Add something like this your js. This is only for frontenders, if you're a backender - stop.

```js

import Vue from 'vue'
import VueI18n from 'vue-i18n'

Vue.use(VueI18n)

const messages = { current: window.django.catalog || {} }

Vue.prototype._ = function() {
  return this.$t.apply(this, arguments)
}

export const i18n = new VueI18n({
  locale: 'current',
  messages,
  silentTranslationWarn: true
})

```
