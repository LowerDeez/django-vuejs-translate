from django.conf.urls import url

from .views import JavaScriptCatalogView

app_name = 'vuejs_translate'

urlpatterns = [
    url(r'^i18n/$', JavaScriptCatalogView.as_view(), name='js-i18n'),
]
