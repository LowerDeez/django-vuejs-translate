-i https://pypi.org/simple
--extra-index-url https://pypi.python.org/simple
django>=1.10
polib==1.1.0
pytz==2018.5
